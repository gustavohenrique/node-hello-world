FROM node:7-alpine
COPY . /src
WORKDIR /src
CMD npm start
